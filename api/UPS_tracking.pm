{
package UPS_tracking;
	use strict;

our $ups_api_key=q|AC918AC7142D9A30|;
our $ups_api_user=q|rhc_rperry|;
our $ups_api_pass=q|yist_467a|;
our @hide_these=qw(FIRS2252 FIRS2248 UPS-2258 FIRS2254 EXPR224F UPS-224C PRIO2257 FEDE2256 UPS-224B 4FF8 EXPR2250);
use WebService::UPS::Activity;
use WebService::UPS::Address;
use WebService::UPS::TrackedPackage;
use WebService::UPS::TrackRequest;
use WebService::UPS::TrackRequest_MI;

use XML::XML2JSON;


sub getUPSPackageInfo {
	my ($dbh, $errs, $self) = @_;
       
        my $Package = WebService::UPS::TrackRequest_MI->new;
        $Package->Username($ups_api_user);
        $Package->Password($ups_api_pass);
        $Package->License($ups_api_key);
        my $tn=$self->{tracking_num};
#         if ($tn =~m/^9$/) {
# 	        my $zip=$dbh->selectrow_array(q{SELECT zip FROM ups_tracking WHERE usps_tracking=?}, undef, ($tn));
#     	    $tn = 420 . $zip . $tn;
#         }
        $Package->TrackingNumber($tn);
        my $trackedpackage = $Package->requestTrack();
        my $trackedpackagexml = $Package->requestTrack_xml();		
		if($trackedpackage->isError()) {
			push(@$errs, {msg => $trackedpackage->getError() , type => 'error' , xml => $trackedpackagexml});
			return $trackedpackage->_returned_xml();
		}


my $da;
	if (my $address=$trackedpackage->getDestinationAddress()) {
	
		$da={	
				city =>    $address->getCity(),
				state =>    $address->getState(),
				zip =>    $address->getZip(),
				address1 =>    $address->getAddressLine1(),
				address2 =>    $address->getAddressLine2()
			};
	        my $frt = { 	raw_destination => $address->_address_hash(), 
	        				destination => $da,
	        				status => $trackedpackage->getCurrentStatus(),
	        				delievery_date => $trackedpackage->getScheduledDeliveryDate(),
	        				weight => $trackedpackage->getWeight(),
	        				raw =>$trackedpackage->_returned_xml() ||   XML::XML2JSON->xml2json($trackedpackage->_returned_xml())
	        				};
	        				

			#format times and date - make async
	 $frt->{raw}{refType}= (ref $frt->{raw}{Shipment}{Package}{Activity});
		if (ref $frt->{raw}{Shipment}{Package}{Activity} eq 'ARRAY') {
				for my $act (@{$frt->{raw}{Shipment}{Package}{Activity}}	) {
					$act->{Time}=~m/^(\d{2})(\d{2})(\d{2})$/;
					my $hr=$1;
					my $min=$2;
					my $sec=$3;
					my $apm= $hr > 11 ? 'PM' : 'AM';
					$hr = $hr > 12 ? $hr - 12 : $hr;
					if ($hr ==0 ) {$hr=12;}
					
					my $f_time=qq{$hr:$min:$sec};
					
					$act->{Time}=$f_time;
					
					$act->{Date}=~s{\d\d(\d\d)(\d\d)(\d\d)}{$2/$3/$1};
					
					#adj address str:
					my @arecs= grep {!/^$/} ($act->{ActivityLocation}{Address}{City},  $act->{ActivityLocation}{Address}{StateProvinceCode},  $act->{ActivityLocation}{Address}{CountryCode});
					my $astr=join(', ', @arecs);
					$act->{address_str}=$astr;
	
				}
	}
			elsif (ref $frt->{raw}{Shipment}{Package}{Activity} eq 'HASH') {
				for my $act (keys %{$frt->{raw}{Shipment}{Package}{Activity}}	) {
					$act->{Time}=~m/^(\d{2})(\d{2})(\d{2})$/;
					my $hr=$1;
					my $min=$2;
					my $sec=$3;
					my $apm= $hr > 11 ? 'PM' : 'AM';
					$hr = $hr > 12 ? $hr - 12 : $hr;
					if ($hr ==0 ) {$hr=12;}
					
					my $f_time=qq{$hr:$min:$sec};
					
					$act->{Time}=$f_time;
					
					$act->{Date}=~s{\d\d(\d\d)(\d\d)(\d\d)}{$2/$3/$1};
					
					#adj address str:
					my @arecs= grep {!/^$/} ($act->{ActivityLocation}{Address}{City},  $act->{ActivityLocation}{Address}{StateProvinceCode},  $act->{ActivityLocation}{Address}{CountryCode});
					my $astr=join(', ', @arecs);
					$act->{address_str}=$astr;
	
				}
	} else {
	 $frt->{raw}{Shipment}{Package}{refType}= (ref $frt->{raw}{Shipment}{Package}) || ' none ';
	}
		$frt->{raw}{Shipment}{PickupDate}=~s{\d\d(\d\d)(\d\d)(\d\d)}{$2/$3/$1};
		$frt->{raw}{Shipment}{PickupDate}=~s{\d\d(\d\d)(\d\d)(\d\d)}{$2/$3/$1};
		$frt->{raw}{Shipment}{RescheduledDeliveryDate}=~s{\d\d(\d\d)(\d\d)(\d\d)}{$2/$3/$1};
		$frt->{raw}{Shipment}{ScheduledDeliveryDate}=~s{\d\d(\d\d)(\d\d)(\d\d)}{$2/$3/$1};
			if (ref $frt->{raw}{Shipment}{Package}{Activity} eq 'ARRAY') {
				if ( $frt->{raw}{Shipment}{Package}{Activity}[0]{Status}{StatusType}{Description} =~ m/^DELIVERED/i)		{			 $frt->{raw}{Shipment}{Package}{Message}={ Description => 'Delivered on ' .  $frt->{raw}{Shipment}{Package}{Activity}[0]{Date} . ' ' . $frt->{raw}{Shipment}{Package}{Activity}[0]{Time} };        	} else {
				unless($frt->{raw}{Shipment}{Package}{Message}) {$frt->{raw}{Shipment}{Package}{Message}={ Description => '' };}
				}
			}
			
			
	       return $frt; 
	}       
}

sub getPackageContents {
	### note that this function should check for HIPAA violations first and does not
	my ($dbh, $errs, $self) = @_;

	my $contents=$dbh->selectall_arrayref(q{  SELECT p.RxNumber, RemainingRefills, DoctorCode, DrugCode, DrugName, QuantityDisp, Sig, DatePrescribed, DateFilled, price FROM ups_tracking u JOIN RxLookUp rlu ON u.customerID=rlu.patientID JOIN prescriptions p ON rlu.RxNumber=p.RxNumber WHERE u.trackNum=? AND DateFilled=DATE(u.shipDate)  AND p.Voided='NO'  }, { Columns =>{}}, ($self->{track_num} || $self->{tracking_num}));
		
		return $contents || $self;
}

# user funcs

###	"addUser" => {authLevel => 15, func => sub {return RD3::addUser(@_);} },
###	"editUser" => {authLevel => 2, func => sub {return RD3::editUser(@_);} },
###	"getUser" => {authLevel => 2, func => sub {return RD3::getUser(@_);} },
	



1;
}











